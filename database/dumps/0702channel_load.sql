-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 07 2019 г., 03:16
-- Версия сервера: 8.0.13
-- Версия PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `smsc_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `channel_load`
--

CREATE TABLE `channel_load` (
  `id` int(11) NOT NULL,
  `channel_name` varchar(255) NOT NULL,
  `channel_load` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `channel_load`
--

INSERT INTO `channel_load` (`id`, `channel_name`, `channel_load`, `updated_at`) VALUES
(1, 'EASYSMPP01', 600, '2019-02-06 10:05:16'),
(4, 'EASYSMPP02', 600, '2019-02-06 10:16:57'),
(5, 'EASYSMPP03', 600, '2019-02-06 10:05:32'),
(6, 'EASYSMPP04', 600, '2019-02-06 10:05:32');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `channel_load`
--
ALTER TABLE `channel_load`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `channel_load`
--
ALTER TABLE `channel_load`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
