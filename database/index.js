//mysql db functions

 function DB(config,linesCount) {
    this.host                         = config.host;
    this.login                        = config.login;
    this.password                     = config.password;
    this.db_name                      = config.db_name;
    this.sms_to_send_table_name       = config.sms_to_send_table_name;
    this.bindings_table_name          = config.bindings_table_name;
    this.logs_table_name              = config.logs_table_name;
    this.linesCount                   = linesCount
    
    
var dbconn = require('knex')({
    client: 'mysql',
    connection: {
      host :        this.host,
      user :        this.login,
      password :    this.password,
      database :    this.db_name
    }
  });  
  
 this.setSMSendedStatus = async function (sqlId, smId){
     return   dbconn('sms_to_send').where('id', sqlId).update({
        sended:'true',
        msg_id: smId,
        status:'SUBMTD'
    })
    .then((rowNum)=>
    {
        return rowNum
    })
    .catch((error)=>{
        return false
    })
 } 

this.addDeliveredSMS = async function(smsObj){
    return dbconn.insert({dlr_from: smsObj.from, dlr_to:smsObj.to, dlr_message:smsObj.message}).into('delivrd_messages')
}

this.getDeliveredSMS = async function(){
    return dbconn('delivrd_messages')
}

 this.setSMSDeliveredStatus = async function (dlr){
     //console.log(dlr)
     smId = dlr.dlrObj.smsId
     smStatus = dlr.dlrObj.smsStat
    return   dbconn('sms_to_send').where('msg_id', smId).update({
        status:smStatus,
        error:smsError
 })
    .then((numberOfUpdatedRow)=>{
        return numberOfUpdatedRow
    })
    .catch((error)=>{
        console.log(error)
        return false
    })}

async function chatObjectAssign(inp,out){
    var allMessagesArray =[];
    var i;
    for (i = 0; i < inp.length; i++) {
    var inpObj = {}
    inpObj.type = 'incoming',
    inpObj.text = inp[i].dlr_message
    inpObj.date = inp[i].created_at
    allMessagesArray.push(inpObj);
    }
    var g;
    for (g = 0; g < out.length; g++) {
        var outObj = {};
        outObj.type = 'outgoing'
        outObj.text = out[g].text;
        outObj.date =out[g].date;
        allMessagesArray.push(outObj)
    }
   
    var sortedAllMessagesArray =allMessagesArray.sort(function(a,b){
        return new Date(b.date) - new Date(a.date);
      });
    return sortedAllMessagesArray.reverse()
}
 //getting chat by sender numbers (all IO sms by number)
 this.getChatBySenderNumber= async function(senderNumber){
    var convertedSenderNum = '8'+senderNumber.substr(2);
    var nonConvertedSenderNum = senderNumber;
    var incomingMessages = await dbconn('delivrd_messages').where('dlr_from',nonConvertedSenderNum)
    var outgoingMessages = await dbconn('sms_to_send').where('phone', convertedSenderNum);
    //allMessages = incomingMessages + outgoingMessages;
   // console.log('allMsg')
    
    return chatObjectAssign(incomingMessages,outgoingMessages);


 }
    //available amount of sms
this.getAmountOfAvailableSMSPerChannel = async function(channel){
    return dbconn('channel_load')
    .where('channel_name',channel)
    .first()
    .returning('channel_load')
}
this.updateDecrementAmountOfAvailableSMSPerChannel = async function(channel,messagesInMessage){
    return dbconn('channel_load')
    .where('channel_name',channel)
    .update({ 'updated_at': new Date(), 'channel_load': dbconn.raw('channel_load -'+messagesInMessage) });
}
this.clearAmountOfAvailableSMSPerChannel = async function(channel){

}
this.getSmsToSend = async function(channel){
   return dbconn('sms_to_send')
            .where('channel',channel)
            .where('sended','false')
            .where('sending','false')
            .first()
  .then((row)=>{
    if(row===undefined){
        return false
    }
    if (row){
        return row
    }
    return false
  })
  .catch((err)=>{
    console.log(err)
  })
}
this.searchAttachment = function (number){//return number of channel or false
  return  dbconn('attachments').where('number',number).first()
    .then((response)=>{
        if (response==undefined){
           return false
        }
        else{
           return response.channel
        }
    })
}
function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    return Math.round(rand);
  }

this.addAttachment = async function (number){
var now = new Date();
if (this.linesCount == 0) return false;
var num = randomInteger(0,this.linesCount-1)
return dbconn.insert({number: number, channel:num, date:now}).into('attachments')
                        .then(()=>{
                            return num;
                        })
                        .error((error)=>{
                            console.error(error)
                        })
                    }
this.newSession = function(msg){
    var now = new Date();
    return  dbconn.insert({msg:msg,date:now})
    .returning('id')
    .into('sessions')
    .then((id)=>{
        return id
    })
}
this.getSessions = function(){
    return dbconn('sessions')
}
this.resendNoDlr = async function(session_id){
   return  await  dbconn('sms_to_send')
    .where('session_id',session_id)
    .where('sended','true')
    .where('status','UNDELIV')
    .update({
        status:'',
        sended:'false',
        sending:'false'
        })
    .then((response)=>{
        return response})
}
this.getUndeliveredMsgs = async function(session_id){
   return  await  dbconn('sms_to_send')
    .where('session_id',session_id)
    .where('status','UNDELIV')
    .then((undeliveredMsg)=>{
        return undeliveredMsg})
}
this.getSessionInfo = async function(session_id){
   var allForSendCount = await  dbconn('sms_to_send')
    .where('session_id',session_id)
    .then((forSendCount)=>{return forSendCount.length})
   var sentMessages  = await dbconn('sms_to_send')
    .where('session_id',session_id)
    .where('sended','true')
    .then((sentMessages)=>{return sentMessages.length})
   var dlrMessages = await dbconn('sms_to_send')
     .where('session_id',session_id)
     .where('status','DELIVRD')
     .then((dlrMessages)=>{return dlrMessages.length})
   return ({session_info:{session_id,allForSendCount,sentMessages,dlrMessages}})

}
this.addMsgToQueue = function (number,message, session_id){
    var now = new Date();
   return this.searchAttachment(number)
    .then((channel)=>{
        if (channel==false){
          return  this.addAttachment(number)
            .then((channel)=>{
              return  dbconn.insert({ session_id:session_id, msg_id:0,priority:0,sended:'false',sending:'false',channel:channel,phone:number,text:message, status:'SUBMTD',error:'none', date:now, update_date:now})
                .into('sms_to_send')
                .then(()=>{return true})
                .catch((err)=>{   
                    console.log(err)
                    return false
                })
    
            })

        }
        else {
          return  dbconn.insert({session_id:session_id,msg_id:0,priority:1,sended:'false',sending:'false',channel:channel,phone:number,text:message, status:'SUBMTD',error:'none', date:now, update_date:now}).into('sms_to_send')
          .then((resp)=>{return true})  
          .catch((err)=>{
                console.log(err)
            })
        }
    })

}


  };
  
  
module.exports=DB;
