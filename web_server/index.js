const express =                 require("express")
const hbs =                     require("hbs");
const bodyParser =              require("body-parser");
const urlencodedParser =        bodyParser.urlencoded({extended: false});
const expressHbs =              require("express-handlebars");
const DB =                      require("./../database/index")
const CONFIG =                  require("./../config/mainConfig.json")
const multer =                  require('multer');
const csv =                     require('fast-csv');
var favicon =                   require('serve-favicon')
var dbconn =                    new DB(CONFIG.mysql_db,CONFIG.lines_count);
var path =                      require('path')
const upload = multer({ dest: 'tmp/csv/' });
const fs = require('fs');


function WebServer(config) {

   this.start = function (){
    const app = express();
    console.log('--S-- Web Server is started')
    app.set("view engine", "hbs");
    app.set("views", __dirname +"/views");
    app.engine("hbs", expressHbs(
        {   
            helpers: {
                test: function () { return "Lorem ipsum" },
                json: function (value, options) {
                    return JSON.stringify(value);
                },
                beatydate: function(value, options){
                    let dt = new Date(value)
                    monthA = 'января,февраля,марта,апреля,мая,июня,июля,августа,сентября,октября,ноября,декабря'.split(',');
                    stringy = dt.getDate()+' '+monthA[dt.getMonth()]+'  '+dt.getHours()+':'+dt.getMinutes()
                    return stringy
                },
                getlinenum: function(value, options){
                    // var linesNames
                    // for (var i = 0; i < CONFIG.lines.length; i++) {
                    //     linesNames = ' '+CONFIG.lines[i].name
                    // }
                    return CONFIG.lines.length
                }
            },
                
            layoutsDir: __dirname +"/views/layouts", 
            partialsDir:__dirname +"/views/partials",
            defaultLayout: "layout",
            extname: "hbs"
        }
    ))
    app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
    app.use(express.static(path.join(__dirname, 'public')));
    app.use((req, res, next) => {

        // -----------------------------------------------------------------------
        //мидлваре авторизации
      
        const auth = {login: CONFIG.web_server.login, password: CONFIG.web_server.password} // change this
      
        // парсим логины и пароли ваши с головы
        const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
        const [login, password] = new Buffer(b64auth, 'base64').toString().split(':')
      
        // проверяем
        if (!login || !password || login !== auth.login || password !== auth.password) {
          res.set('WWW-Authenticate', 'Basic realm="401"') // нет доступа
          res.status(401).send('Необходимо авторизоваться') // скажем что нужен доступ
          return
        }
      
        // -----------------------------------------------------------------------
        // доступ есь
        next()
      
      })
    app.get("/sessions/:session_id/", (request,response)=>
    {
        const session_id = request.params.session_id;
        dbconn.getSessionInfo(session_id)
        .then((session_info)=>{
            var sessionInfo = session_info.session_info;
            response.render("session_info",{
                timeBeforeExec:CONFIG.main_timer_ms/1000*(sessionInfo.allForSendCount-sessionInfo.sentMessages),
                id: session_id,
                title:'Информация о сессии '+session_id,
                all:sessionInfo.allForSendCount,
                sended: sessionInfo.sentMessages,
                delivered: sessionInfo.dlrMessages
            })
        })
    })
    app.get("/sessions/:session_id/undelivered", (request,response)=>
    {
        const session_id = request.params.session_id;
        dbconn.getUndeliveredMsgs(session_id)
        .then((undeliveredMessages)=>{
            response.render("undelivered",{
                id: session_id,
                title:'Недоставленные сообщения сессии  '+session_id,
                undeliveredMessages,
                count:undeliveredMessages.length
            })
        })
    })
    app.get('/send_multiple/csv',(request,response)=>{
        response.render("csv",
        {
            title:'Рассылка из CSV',
            
        })
    } )
    app.get('/auth',(request,response)=>{
        response.render("auth/auth",
        {
            title:'Авторизация',
            layout:'auth'
        })
    } )
    app.post('/send_multiple/csv', upload.single('csv'), function (request, response) {
        const fileRows = [];
      
        // open uploaded file
        csv.fromPath(request.file.path)
          .on("data", function (data) {
            fileRows.push(data); // push each row
          })
          .on("end", function () {
            if(!request.body) return response.sendStatus(400);
            dbconn.newSession(request.body.message)
            .then((session_id)=>{
            parseAndSendCSV(fileRows, request.body.message, session_id)
            .then((resp)=>{
                response.render("sended_success",{
                    text:'Количество номеров ',
                    number:resp,
                    message:request.body.message,
                    session_id:session_id
                });
            })
        })
            fs.unlinkSync(request.file.path);   // remove temp file
            //process "fileRows" and respond
          })
      });
      app.get('/chatroom/:sender',(request,response)=>{
        const sender = request.params.sender;
        dbconn.getChatBySenderNumber(sender)
        .then((chatRows)=>{
            response.render("chatroom",
            {
                title:sender,
                tel:parseSenderNum(sender),
                chatRows,
                //searchElem:getSearchContent()
            })
        })

    } )
    app.get("/sessions/:session_id/resend_nodlr", (request,response)=>
    {
        const session_id = request.params.session_id;
        dbconn.resendNoDlr(session_id)
        .then((count)=>{
            var title = 'Готово!'
            var message =  count + ' недоствленных сообщений принято на обработку'
            if (count == 0){
                titlle = "Ошибка"
                message = "Все сообщения были доставлены. Отправлять нечего :("
            }
            response.render("ok",{
                title:title,
                message: message,
            })
        })
    })
    app.use("/reports", function(request, response){
        dbconn.getSessions()
        .then(sessions=>{
            response.render("sessions",{
                title:'Отчеты по сессиям',
                sessions
            })
        })
        
    });

    app.use("/delivrd_sms", function(request, response){
        dbconn.getDeliveredSMS()
        .then(delivrdSMS=>{
            response.render("delivrd_sms",{
                title:'Полученные смс',
                delivrdSMS
            })
        })
        
    });

    app.get("/send_sms", urlencodedParser, function (request, response) {
        
        response.render("send_sms",{
            title:"Отправить смс"
        });
    });
    app.get("/send_multiple", urlencodedParser, function (request, response) {
        
        response.render("send_multiple");
    });
    app.get("/3d", urlencodedParser, function (request, response) {
        
        response.render("3d");
    });
    
    app.post("/send_multiple",urlencodedParser,function(request,response){
        if(!request.body) return response.sendStatus(400);
        dbconn.newSession(request.body.msg)
        .then((session_id)=>{
        parseAndSend(request.body.numbers, request.body.msg, session_id)
        .then((resp)=>{
            response.render("sended_success.hbs",{
                text:'Количество номеров ',
                number:resp,
                message:request.body.msg,
                session_id:session_id
            });
        })
    })
    })

    app.post("/send_sms", urlencodedParser, function (request, response) {
        if(!request.body) return response.sendStatus(400);
      //  dbconn.newSession(request.body.msg)
       // .then((session_id)=>{
        dbconn.addMsgToQueue(request.body.tel, request.body.msg, 0)
        .then((sm_resp)=>{
                response.render("sended_success.hbs",{
                    text:'Номер ',
                    number:request.body.tel,
                    message:request.body.msg,
                    session_id:0
                });
            
       // })
    })
        
    });
    app.use("/", function(request, response){
         
        response.render("main",
        {
            title:'Главная',

        })
    });
    app.listen(CONFIG.web_server.port);}
}

async function parseAndSend(numbers,msg, session_id){
    var message = msg;
    var array = numbers.split(';')
    var filtredArray = array.filter(function(number) {
        return number.length > 9;
      });
    filtredArray.forEach(function(item) {
        dbconn.addMsgToQueue(item, message,session_id)
      });
    return filtredArray.length
}

async function parseAndSendCSV(numbers,msg, session_id){ //async addMsg, returning msg count
    var message = msg;
    var array = numbers
    addMsgInterval(array,message,session_id);
    return array.length
}

function addMsgInterval(arr, message,session_id) { //timer-based async (freaking mind)
    var i = 0;
    var iteractionsCount = arr.length
   
    var timerId = setInterval(function() { 
        if (i == iteractionsCount) clearInterval(timerId);
        //console.log(arr[i], message,session_id)
        // v//ar testEx = arr[i].toString();
      if (arr[i]==undefined){
        //  console.log('Пустая строка!')
      }else{
         //console.log('***',arr[i], message,session_id)
        dbconn.addMsgToQueue(arr[i], message,session_id)
      }

      i++;
    }, 1);
  }
  function parseSenderNum(number){
      return number.replace("+7","8")
  }
function getAndParseLineInformation(){ //getting all information for dshboard view
    var activeLines = 0;
    var untilTheEndOfWorkms = 0;
    var smsInQueue = 0;
    var simBalance = 0;
    var lineLoad = 0;
    var permDailyLoad = 0;
    var linesInfo = [];
    //------------------//
    var configLines = CONFIG.lines;

    for (var i = 0; i < configLines.length; i++) {
        var linesInformationObject = {}
        linesInformationObject[i]={
            name:configLines[i].name,
            dailyLoad:configLines[i].perm_daily_load,
            number:configLine[i].number,
            active:'YES',
            lineLoad:dbconn.getLineLoad(name),
            lineAvailableLoad:dbconn.getLineAvailableLoad(name),
            balance: 'Достаточно',
        }
     }
     return linesInformationObject

}

module.exports = WebServer;