//var redis = require("redis"),
//    redisClient = redis.createClient();
const {Worker} = require('worker_threads');
const DB = require ('./database/index')
const CONFIG = require ('./config/mainConfig.json')
const WebServer = require ('./web_server/index')
var Server = new WebServer();
var cron = require('node-cron');
var db = new DB(CONFIG.mysql_db);
var workerThreads = {};
linesCount = CONFIG.lines.length;
console.log('--M-- Master is started')
Server.start();

function workerObserver(config,index){

}


function startWorker(config,path) {
   // console.log(standByTimer)
    let w = new Worker(path, {workerData: config});   
      w.on('message', (msg) => {
        if (msg.type=='pong'){
        }
        if (msg.type=='send_success'){
        console.log('--M-- message sended MSG ID', msg.id)
        console.log(msg)
            db.setSMSendedStatus( msg.mess_id,msg.id)
        }
        if (msg.type == 'sms_recieved'){
            console.log('--M-- SMS recieved!', msg.smsObj)
            db.addDeliveredSMS(msg.smsObj)
        }

        if (msg.type=='send_error_too_many'){
        console.log('--M-- SMS queue buffer overflow! Resending')
            //db.stackoverflow(msg.id)
        }

        if (msg.type =='dlr_recieved'){
            console.log('--M-- DLR recieved. MSG ID ' , msg)
            db.setSMSDeliveredStatus(msg)
        }
        if (msg.type =='sending_error'){
            console.log('--M-- Sending error ' , msg)
            //db.updateSMSStatus(msg.dlr)
        }
        if (msg.type =='dlr_error'){
            console.log('--M-- All dlr per message not recieved! ' , msg)
            //db.updateSMSStatus(msg.dlr)
        }
      });
      
      w.on('error', (err) => {
        console.error('--M-- recieve worker error code:', err);
        return {event:'error', code:err}
      });
      
      w.on('exit', (code) => {
        return {event:'exit', code:err}
      });
    return w;
}

var i = 0;
while (i < linesCount) {
    let worker = startWorker(CONFIG.lines[i],__dirname + '/smpp_worker/worker.js', (err, result) => {
        if(err) return console.error(err);
    })
    
    let config = CONFIG.lines[i]
    let number = i
    let timer = setInterval(function(){workerLoop(config,number)}, CONFIG.main_timer_ms);
    let observer = workerObserver(config)
    workerThreads[i]={worker, config, timer,i,observer};
  i++;
}

function isDayTime(){
    var hours = new Date().getHours()
    const isDayTime = hours > CONFIG.start_hour && hours < CONFIG.end_hour
    return isDayTime
}


 
cron.schedule('01 09 * * *', () => {
  console.log('Clear All Lines');
  var i = 0;
  var confLines = CONFIG.lines;
  var linesCount = confLines.length;
  while (i < linesCount) {
    db.clearAmountOfAvailableSMSPerChannel(confLines[i]);
    i++
  }

});
//var timerClearCons = setInterval(function clearTimeOut(){console.log('\033[2J')}, CONFIG.main_timer_ms-100);

  function workerLoop(config,channelId){ //timer loop function, looking sms for send
    var channel = channelId; 
   if (isDayTime() == false){
       console.log('Night at the yard. '+config.name+'  is sleeping')
   }
   else{
    db.getSmsToSend(channel)
    .then((smsToSend)=>{
        if (smsToSend == false){
            console.log('--M-- '+config.name+' say "Not for sending. Waiting for '+CONFIG.main_timer_ms+' milliseconds" ')
            return
        }
        else{
            var channelName = getChannelNameByID(smsToSend.channel)

            db.getAmountOfAvailableSMSPerChannel(channelName)
            .then((channelLoadObj)=>{
                var messagesInMessage = Math.ceil(smsToSend.text.length/70);
                var acceptableSMS = channelLoadObj.channel_load;
                if (acceptableSMS > 0){
                    console.log(channelName,'отправка')
                    db.updateDecrementAmountOfAvailableSMSPerChannel(channelName,messagesInMessage)
                    redisClient.set(smsToSend.channel, "inuse", redis.print);
                    workerThreads[smsToSend.channel].worker.postMessage({command:'send', addr:smsToSend.phone, message:smsToSend.text, id:smsToSend.id})
                }
                else {
                    console.log(channelName+' достигнут лимит, продолжение рассылки в 10 утра')
                    return
                }

            })
            

       }
   })
   }

    }
function getChannelNameByID(channel_id){
return CONFIG.lines[channel_id].system_id
}
    



