const { parentPort, workerData, postMessage } = require('worker_threads');
const smpp = require('smpp');
const CONFIG = workerData;
const log = console.log;
const session = smpp.connect('smpp://'+CONFIG.ip+':'+CONFIG.port);
session.bind_transceiver({
	system_id: CONFIG.system_id,
    password: CONFIG.password,
    deliveryReport:true
}, pdu => bindingTransmitter(pdu));

function sendMessageResolver(to,message, smsId){
    messageLen = message.length;
    if (messageLen < 70){
        sendShortMessage(to, message, smsId);
        return
    }
    else{
        sendLongMessage(to,message, smsId);
        return
    }
}

function sendShortMessage(to,message, smsId){
    session.submit_sm({
        destination_addr: to,
        registered_delivery: true,
        short_message:message
    }, function(pdu) {
            if (pdu.command_status == 0){
                submitTransmitter(pdu, smsId)
            }
    });
}

function sendLongMessage(to,message, smsId){
    session.submit_sm({
        destination_addr: to,
        registered_delivery: true,
        message_payload: message
    }, function(pdu) {
            if (pdu.command_status == 0){
                submitTransmitter(pdu, smsId)
            }
    });
}

function submitTransmitter(submitPDU, smsId){
    parentPort.postMessage({type:'send_success',mess_id:smsId,id:submitPDU.message_id}); 
}
function bindingTransmitter(pdu){
    console.log('--W-- Transciever binding complete')
    parentPort.postMessage({type:'bind_success',pdu})
}

function deliverTransmitter(pdu){
    console.log(pdu)
    if (pdu.destination_addr.length > 1){
        var from =pdu.source_addr;
        if (from.length < 5){return}//отбросим стремные смс
        var to = pdu.destination_addr;
        var message = pdu.short_message.message ;
        parentPort.postMessage({type:'sms_recieved', smsObj:{from, to,message}})
        return
    }
    try {
    var pduShortInArray = pdu.short_message.message.split(' ')
    var smId   = pduShortInArray[0].split(':')[1]
    var smStat =pduShortInArray[7].split(':')[1]
    var smErr =pduShortInArray[8].split(':')[1]
    log(smId, smStat, smErr)
     var dlrObj  = {
         smsId:smId,
         smsStat: smStat,
         smsError:smErr
     }
     parentPort.postMessage({type:'dlr_recieved',dlrObj})
    }
     
     catch (error){
         console.log('DLR Stringify error!',error)
         return false;
     }
    
}

function errorTransmitter(error){
    console.log('Worker error!')
    parentPort.postMessage({type:'session_error',error})
}


session.on('error',     error =>  errorTransmitter  (error))
session.on('deliver_sm',dlrPdu => deliverTransmitter(dlrPdu))

//078
//*105#



//workersObservers
parentPort.on('message',(value)=>{
    if (value.command == 'send'){
      log('--W--Отправить "'+value.message+'" на '+value.addr+' с линии '+CONFIG.name)
     sendMessageResolver(value.addr,value.message, value.id)
    }
    else if(value.command =='ping'){
     parentPort.postMessage({type:'pong'});
    }
    })